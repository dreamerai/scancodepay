<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>登陆</title>

        <link href="../static/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="../static/css/bootstrap.min.css" rel="stylesheet">
        <link href="../static/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css"
              rel="stylesheet">

        <link href="../static/css/app.min.1.css" rel="stylesheet">
        <link href="../static/css/app.min.2.css" rel="stylesheet">
        <link href="../static/css/layui.css" rel="stylesheet">
        <link href="../static/css/style.css" rel="stylesheet">
    </head>
    
    <body  onkeydown="keyLogin()">

    <div class="container">
        <form class="form-signin">
            <h2 class="form-signin-heading">码闪付管理后台</h2>
            <div class="login-wrap">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="用户名/邮箱" id="username">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="密码" id="password">
                </div>

                <div class="form-group" id="vcode">
                    <input type="text" name="vcode" placeholder="请输入验证码" style="width: 130px;height: 42px;">
                    <img src="/getGifCode"/>
                </div>
                <button class="btn btn-lg btn-login btn-block" type="button" onclick="login()">登录</button>
            </div>

        </form>

    </div>
    <script src="../static/vendors/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../static/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../static/vendors/bower_components/Waves/dist/waves.min.js"></script>
    <script src="../static/js/functions.js"></script>
    <script src="../static/js/layui.all.js"></script>

    </body>

<script>


    jQuery(document).ready(function () {
        //动态验证码
        $("#vcode").on("click", 'img', function () {
            /**动态验证码，改变地址，多次在火狐浏览器下，不会变化的BUG，故这样解决*/
            var i = new Image();
            i.src = '/getGifCode?' + Math.random();
            $(i).replaceAll(this);
        });
    });

    function login() {

        var account = $("#username").val();
        var password = $("#password").val();
        var vcode = $("input[name=vcode]").val();

        if (account == null || account == "") {
            return layer.msg('用户名不能为空！', function () {
            }), !1;
        }
        if (password == null || password == "") {
            return layer.msg('密码不能为空！', function () {
            }), !1;
        }
         if(vcode==null||vcode==""){
             return layer.msg('验证码不能为空！',function(){}),!1;
         }
         if($('[name=vcode]').val().length !=4){
             return layer.msg('验证码的长度为4位！',function(){}),!1;
         }

        $.ajax({
            type: "post",
            url: "/login/main",
            data: {"username": account, "password": password, "vcode": vcode},
            cache: false,
            success: function (data) {
                if (data.message == "success") {
                    parent.location.href = '/index.html';
                } else {
                    layer.msg(data.message, function () {
                    }), !1;
                }
            }, error: function (data) {
                layer.msg(data.message, function () {
                }), !1;
            }
        });

    }

    function keyLogin() {
        if (event.keyCode == 13) {
            login();
        }
    }


</script>
</html>