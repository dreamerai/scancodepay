<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>码闪付管理后台</title>


        <link href="${rc.contextPath}/static/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css"
              rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css"
              rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css"
              rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css"
              rel="stylesheet">
        <link href="${rc.contextPath}/static/css/app.min.1.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/css/app.min.2.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/css/semantic.min.css" rel="stylesheet">
        <link rel="stylesheet" href="${rc.contextPath}/static/sys/layui/css/layui.css" media="all" />
        <link rel="stylesheet" href="${rc.contextPath}/static/sys/css/user.css" media="all" />

        <script src="${rc.contextPath}/static/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="${rc.contextPath}/static/vendors/sparklines/jquery.sparkline.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="${rc.contextPath}/static/js/flot-charts/curved-line-chart.js"></script>
        <script src="${rc.contextPath}/static/js/flot-charts/line-chart.js"></script>
        <script src="${rc.contextPath}/static/js/charts.js"></script>
        <script src="${rc.contextPath}/static/js/charts.js"></script>
        <script src="${rc.contextPath}/static/js/functions.js"></script>
        <script src="${rc.contextPath}/static/js/demo.js"></script>
        <script src="${rc.contextPath}/static/js/semantic.min.js"></script>

    </head>


