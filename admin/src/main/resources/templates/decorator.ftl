<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
    <#include "header.ftl">
    <body>
    <header id="header" class="clearfix" data-current-skin="blue">
        <ul class="header-inner">
            <li id="menu-trigger" data-trigger="#sidebar">
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>

            <li class="logo hidden-xs">
                <a href="index.html">码闪付管理后台</a>
            </li>

            <li class="pull-right">
                <ul class="top-menu">
                    <li id="toggle-width">
                        <div class="toggle-switch">
                            <input id="tw-switch" type="checkbox" hidden="hidden">
                            <label for="tw-switch" class="ts-helper"></label>
                        </div>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                        <ul class="dropdown-menu dm-icon pull-right">
                            <li class="skin-switch hidden-xs">
                                <span class="ss-skin bgm-lightblue" data-skin="lightblue"></span>
                                <span class="ss-skin bgm-bluegray" data-skin="bluegray"></span>
                                <span class="ss-skin bgm-cyan" data-skin="cyan"></span>
                                <span class="ss-skin bgm-teal" data-skin="teal"></span>
                                <span class="ss-skin bgm-orange" data-skin="orange"></span>
                                <span class="ss-skin bgm-blue" data-skin="blue"></span>
                            </li>
                            <li class="divider hidden-xs"></li>
                            <li class="hidden-xs">
                                <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
                            </li>
                            <li>
                                <a data-action="clear-localstorage" href=""><i class="zmdi zmdi-delete"></i> Clear Local Storage</a>
                            </li>
                            <li>
                                <a href=""><i class="zmdi zmdi-face"></i> Privacy Settings</a>
                            </li>
                            <li>
                                <a href=""><i class="zmdi zmdi-settings"></i> Other Settings</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>

    </header>

    <section id="main" data-layout="layout-1">
        <aside id="sidebar" class="sidebar c-overflow">
            <div class="profile-menu">
                <a href="">
                    <div class="profile-pic">
                        <img src="${rc.contextPath}/static/img/profile-pics/1.jpg" alt="">
                    </div>

                    <div class="profile-info">
                      个人信息

                        <i class="zmdi zmdi-caret-down"></i>
                    </div>
                </a>

                <ul class="main-menu">
                    <li>
                        <a href="profile-about.html"><i class="zmdi zmdi-account"></i> 显示详情</a>
                    </li>
                    <li>
                        <a href=""><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a>
                    </li>
                    <li>
                        <a href=""><i class="zmdi zmdi-settings"></i> 设置</a>
                    </li>
                    <li>
                        <a href=""><i class="zmdi zmdi-time-restore"></i> 退出登陆</a>
                    </li>
                </ul>
            </div>

            <ul class="main-menu">
                <li class="active">
                    <a href="index.html"><i class="zmdi zmdi-home"></i> 首页</a>
                </li>
                <li class="sub-menu">
                    <a href=""><i class="zmdi zmdi-view-compact"></i> 用户管理</a>

                    <ul>
                        <li><a href="/admin/system/user/list"  target="pageframe">用户列表</a></li>
                        <li><a href="/admin/system/role/list"  target="pageframe">角色列表</a></li>
                        <li><a href="/admin/system/menu/list"  target="pageframe">权限列表</a></li>
                    </ul>
                </li>
                <li><a href="typography.html"><i class="zmdi zmdi-format-underlined"></i> 代理商管理</a></li>
                <li class="sub-menu">
                    <a href=""><i class="zmdi zmdi-widgets"></i>商户管理</a>
                    <ul>
                        <li><a href="/admin/platform/mer/list">商户列表</a></li>
                        <li><a href="/test" target="pageframe">商户操作日志</a></li>
                        <li><a href="/admin/platform/mer/table">上游商户列表</a></li>
                    </ul>
                </li>

            </ul>
        </aside>
        <!-- 其他页面嵌套 -->
         <sitemesh:write property='body'/>
        <footer id="footer">
        Copyright &copy; 2018 码闪付
        <ul class="f-menu">
            <li><a href="">首页</a></li>
            <li><a href="">关于我们</a></li>
            <li><a href="">联系我们</a></li>
        </ul>
    </footer>



    <div class="page-loader">
        <div class="preloader pls-blue">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>

            <p>正在加载...</p>
        </div>
    </div>
    </body>
  </html>