<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>角色列表</title>
        <link href="${rc.contextPath}/static/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/css/app.min.1.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/css/app.min.2.css" rel="stylesheet">
    </head>
    <body>


            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>角色列表</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>管理员列表<small></small></h2>
                        </div>
                        
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>登陆名称</th>
                                        <th>昵称</th>
                                        <th>邮箱</th>
                                        <th>电话</th>
                                        <th>状态</th>
                                        <th>创建时间</th>
                                        <th>最后登陆时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <#if userList??>
					                 <#list userList as info>
                                    <tr>
                                        <td>${info.loginName}</td>
                                        <td>${info.nickName}</td>
                                        <td>${info.email}</td>
                                           <td>${info.tel}</td>

                                            <#if info.delFlag>
                                                <td>禁用</td>
                                            <#else>
                                                <td>正常</td>
                                            </#if>
                                        <td>${info.createDate?string('yyyy-MM-dd HH:mm:ss')}</td>
                                        <td>${info.updateDate?string('yyyy-MM-dd HH:mm:ss')}</td>
                                        <td>编辑</td>
                                    </tr>
                                     </#list>
                                    </#if>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

        

        <script src="${rc.contextPath}/static/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="${rc.contextPath}/static/js/functions.js"></script>
        <script src="${rc.contextPath}/static/js/demo.js"></script>

    </body>
  </html>