<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>用户列表</title>
        <link href="${rc.contextPath}/static/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/css/app.min.1.css" rel="stylesheet">
        <link href="${rc.contextPath}/static/css/app.min.2.css" rel="stylesheet">
    </head>
    <body>


            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <div clss="well">
                            <div class="form-group" style="margin-bottom: 0px;margin-top: 10px;margin-left: 15px">
                                <a class="btn btn-success" style="background-color:#5160cd;border-color: #515bcd" onclick="$('#addOperator').modal();">增加</a>&nbsp;&nbsp;
                                <a class="btn btn-success" style="background-color:#5160cd;border-color: #515bcd" href="${request.contextPath}/admin/user/userIndex">刷新</a>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>管理员列表<small>你不是真正的快乐</small></h2>
                        </div>
                        
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>登陆名称</th>
                                        <th>昵称</th>
                                        <th>邮箱</th>
                                        <th>电话</th>
                                        <th>状态</th>
                                        <th>创建时间</th>
                                        <th>最后登陆时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <#if userList??>
					                 <#list userList as info>
                                    <tr>
                                        <td>${info.loginName}</td>
                                        <td>${info.nickName}</td>
                                        <td>${info.email}</td>
                                           <td>${info.tel}</td>

                                            <#if info.delFlag>
                                                <td>禁用</td>
                                            <#else>
                                                <td>正常</td>
                                            </#if>
                                        <td>${info.createDate?string('yyyy-MM-dd HH:mm:ss')}</td>
                                        <td>${info.updateDate?string('yyyy-MM-dd HH:mm:ss')}</td>
                                        <td>编辑</td>
                                    </tr>
                                     </#list>
                                    </#if>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <div class="modal fade" id="addOperator" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="margin-top: 180px">
                        <div class="modal-header" style="background: #beb04d">
                            <h4 class="modal-title" id="addroleLabel">添加管理员</h4>
                        </div>
                        <div class="modal-body">
                            <form id="operator" enctype="multipart/form-data" action="${rc.contextPath}/admin/user/addUser"  method="post">
                                <div class="form-group">
                                    <label  class="control-label">管理员名称:</label>
                                    <input type="text" style="height:30px" class="form-control" name="loginName" id="account" placeholder="请输入管理员名称"/>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">登录密码:</label>
                                    <input type="text" style="height:30px" class="form-control" name="password" id="password" placeholder="请输入登陆密码"/>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">确认密码:</label>
                                    <input type="text" style="height:30px" class="form-control" name="rePassword" id="rePassword" placeholder="请再次输入密码"/>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label">选择角色:</label>
                                     <#if roleList??>
                                         <#list roleList as role>
                                     <input type="checkbox" value="${role.id}" id="roleUpdate" name="roleIdList" />${role.name}
                                         </#list>
                                     </#if>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                                    <button type="submit"  class="btn btn-primary" style="background: #ea7137; border-color: #f29365">提交</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


        

        <script src="${rc.contextPath}/static/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="${rc.contextPath}/static/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="${rc.contextPath}/static/js/functions.js"></script>
        <script src="${rc.contextPath}/static/js/demo.js"></script>




    </body>
  </html>