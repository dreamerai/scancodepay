<!DOCTYPE html>
<body>


    <section id="content">
        <div class="container">
            <div class="block-header">
                <div class="form-group" style="margin-bottom: 0px;margin-top: 10px;margin-left: 15px">
                    <a class="btn btn-success" style="background-color:#5160cd;border-color: #515bcd"
                       onclick="$('#addOperator').modal();">增加</a>&nbsp;&nbsp;
                    <a class="btn btn-success" style="background-color:#5160cd;border-color: #515bcd"
                       href="${request.contextPath}/admin/user/userIndex">刷新</a>
                </div>
            </div>

            <div class="card">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                            <#--  <th>代理商</th>-->
                                <th>商户名称</th>
                                <th>商户代码</th>
                                <th>登录昵称</th>
                                <th>联系人</th>
                                <th>联系人号码</th>
                                <th>商户状态</th>
                                <th>审核状态</th>
                                <th>总积分</th>
                                <th>可用积分</th>
                                <th>冻结积分</th>
                                <th>登录绑定IP</th>
                                <th>备注</th>
                                <th>谷歌验证器</th>
                                <th>更新时间</th>
                                <th>是否绑定上游商户</th>
                                <th>上游商户</th>
                                <th>操作</th>

                            </tr>
                            </thead>
                            <tbody>
                                    <#if merList??>
					                 <#list merList as info>
                                    <tr>
                                        <td>${info.BusinessName}</td>
                                        <td>${info.OrgCode}</td>
                                        <td>${info.Username}</td>
                                        <td>${info.Contact}</td>
                                        <td>${info.ContactMobile}</td>
                                        <#if info.Status=="0">
                                                <td>正常</td>
                                        <#else>
                                                <td>禁用</td>
                                        </#if>

                                             <#if info.ApproveStateCode=="0">
                                                <td>通过</td>
                                             <#elseif info.ApproveStateCode=="1">
                                                <td>未审核</td>
                                             <#elseif info.ApproveStateCode=="2">
                                                <td>未通过</td>
                                             </#if>

                                        <td>${info.Amount}</td>
                                        <td>${info.AvailableAmount}</td>
                                        <td>${info.FreezeAmount}</td>
                                        <td>${info.LoginBindIP}</td>
                                        <td>${info.remarks}</td>

                                              <#if info.googleStatus=="0">
                                                <td>已绑定</td>
                                              <#else>
                                                <td>未绑定</td>
                                              </#if>

                                        <td>${info.updateDate?string('yyyy-MM-dd HH:mm:ss')}</td>
                                         <#if info.isBindingUpstreamMer=="0">
                                                <td>未绑定</td>
                                         <#else>
                                                <td>已绑定</td>
                                         </#if>

                                        <td>${info.upstreamMerAccount}</td>
                                        <td>编辑</td>
                                    </tr>
                                     </#list>
                                    </#if>
                            </tbody>
                        </table>
                    ${page}
                    </table>
                </div>
            </div>
        </div>
    </section>







<#--

<section id="content">
    <div class="container">
        <div class="block-header">
            <div clss="well">
                <div class="form-group" style="margin-bottom: 0px;margin-top: 10px;margin-left: 15px">
                    <a class="btn btn-success" style="background-color:#5160cd;border-color: #515bcd"
                       onclick="$('#addOperator').modal();">增加</a>&nbsp;&nbsp;
                    <a class="btn btn-success" style="background-color:#5160cd;border-color: #515bcd"
                       href="${request.contextPath}/admin/user/userIndex">刷新</a>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h2>商户列表
                    <small>你不是真正的快乐</small>
                </h2>
            </div>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                    &lt;#&ndash;  <th>代理商</th>&ndash;&gt;
                        <th>商户名称</th>
                        <th>商户代码</th>
                        <th>登录昵称</th>
                        <th>联系人</th>
                        <th>联系人号码</th>
                        <th>商户状态</th>
                        <th>审核状态</th>
                        <th>总积分</th>
                        <th>可用积分</th>
                        <th>冻结积分</th>
                        <th>登录绑定IP</th>
                        <th>备注</th>
                        <th>谷歌验证器</th>
                        <th>更新时间</th>
                        <th>是否绑定上游商户</th>
                        <th>上游商户</th>
                        <th>操作</th>

                    </tr>
                    </thead>
                    <tbody>
                                    <#if merList??>
					                 <#list merList as info>
                                    <tr>
                                        <td>${info.BusinessName}</td>
                                        <td>${info.OrgCode}</td>
                                        <td>${info.Username}</td>
                                        <td>${info.Contact}</td>
                                        <td>${info.ContactMobile}</td>
                                        <#if info.Status=="0">
                                                <td>正常</td>
                                            <#else>
                                                <td>禁用</td>
                                            </#if>

                                             <#if info.ApproveStateCode=="0">
                                                <td>通过</td>
                                             <#elseif info.ApproveStateCode=="1">
                                                <td>未审核</td>
                                             <#elseif info.ApproveStateCode=="2">
                                                <td>未通过</td>
                                             </#if>

                                        <td>${info.Amount}</td>
                                        <td>${info.AvailableAmount}</td>
                                        <td>${info.FreezeAmount}</td>
                                        <td>${info.LoginBindIP}</td>
                                        <td>${info.remarks}</td>

                                              <#if info.googleStatus=="0">
                                                <td>已绑定</td>
                                              <#else>
                                                <td>未绑定</td>
                                              </#if>

                                        <td>${info.updateDate?string('yyyy-MM-dd HH:mm:ss')}</td>
                                         <#if info.isBindingUpstreamMer=="0">
                                                <td>未绑定</td>
                                         <#else>
                                                <td>已绑定</td>
                                         </#if>

                                        <td>${info.upstreamMerAccount}</td>
                                        <td>编辑</td>
                                    </tr>
                                     </#list>
                                    </#if>
                    </tbody>
                </table>

                ${page}
            </div>
        </div>
    </div>
</section>-->

<div class="modal fade" id="addOperator" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 180px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">添加管理员</h4>
            </div>
            <div class="modal-body">
                <form id="operator" enctype="multipart/form-data" action="${rc.contextPath}/admin/user/addUser"
                      method="post">
                    <div class="form-group">
                        <label class="control-label">管理员名称:</label>
                        <input type="text" style="height:30px" class="form-control" name="loginName" id="account"
                               placeholder="请输入管理员名称"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">登录密码:</label>
                        <input type="text" style="height:30px" class="form-control" name="password" id="password"
                               placeholder="请输入登陆密码"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">确认密码:</label>
                        <input type="text" style="height:30px" class="form-control" name="rePassword" id="rePassword"
                               placeholder="请再次输入密码"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">选择角色:</label>
                                     <#if roleList??>
                                         <#list roleList as role>
                                     <input type="checkbox" value="${role.id}" id="roleUpdate"
                                            name="roleIdList"/>${role.name}
                                         </#list>
                                     </#if>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                        <button type="submit" class="btn btn-primary"
                                style="background: #ea7137; border-color: #f29365">提交
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
</body>
</html>