package com.mashanfu.pay.controller.platform;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mashanfu.pay.entity.OrganizationEntity;
import com.mashanfu.pay.service.MerchantService;
import com.mashanfu.pay.util.page.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


/**
 *  商户控制层
 */
@Controller
@RequestMapping(value = "admin/platform/mer")
public class MerchantController {

    @Autowired
    private MerchantService merchantService;

    /**
     * 查询商户列表
     */
    //权限暂时隐藏
    /*  @RequiresPermissions("sys:user:list")*/
    @RequestMapping("list")
    public String list(String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request){

        modelMap.put("findContent", findContent);
        if (pageSize == null) {
            pageSize = 25;
        }
        /* Map map = WebUtils.getParametersStartingWith(request, "s_");*/
        EntityWrapper<OrganizationEntity> organizationEntityEntityWrapper = new EntityWrapper<>();
       /* if(!map.isEmpty()){
            String keys = (String) map.get("key");
            if(StringUtils.isNotBlank(keys)) {
                organizationEntityEntityWrapper.like("login_name", keys).or().like("tel", keys).or().like("email", keys);
            }
        }*/
    /*    Page<OrganizationEntity> userPage = merchantService.selectPage(new Page<>(),organizationEntityEntityWrapper);*/
        Pagination<OrganizationEntity> pagination= merchantService.getList(pageNo,pageSize,organizationEntityEntityWrapper);
        modelMap.put("merList", pagination.getList());
        modelMap.put("page", pagination.getPageHtml());
        return  "/admin/platform/merIndex";

    }


    @RequestMapping(value = "/table")
    public String getTable(){
        return "/admin/platform/tables";
    }
}
