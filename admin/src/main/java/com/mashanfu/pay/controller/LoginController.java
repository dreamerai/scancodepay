package com.mashanfu.pay.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mashanfu.pay.annotation.SysLog;
import com.mashanfu.pay.base.BaseController;
import com.mashanfu.pay.base.MySysUser;
import com.mashanfu.pay.entity.*;
import com.mashanfu.pay.util.*;
import com.xiaoleilu.hutool.http.HttpUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;

@Controller
public class LoginController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Value("${server.port}")
    private String port;

    @GetMapping("login")
    public String login(HttpServletRequest request) {
        LOGGER.info("跳到这边的路径为:" + request.getRequestURI());
        Subject s = SecurityUtils.getSubject();
        LOGGER.info("是否记住登录--->" + s.isRemembered() + "<-----是否有权限登录----->" + s.isAuthenticated() + "<----");
        if (s.isAuthenticated()) {
            return "redirect:index";
        } else {
            return "login";
        }
    }

    @PostMapping("login/main")
    @ResponseBody
    @SysLog("用户登录")
    public Map<String, Object> loginMain(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String code = request.getParameter("vcode");
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            resultMap.put("status", "500");
            resultMap.put("message", "用户名或者密码不能为空");
            return resultMap;
        }

        if (StringUtils.isBlank(code)) {
            resultMap.put("status", "500");
            resultMap.put("message", "验证码不能为空");
            return resultMap;
        }
        Map<String, Object> map = Maps.newHashMap();
        String error = null;
        HttpSession session = request.getSession();
        if (session == null) {
            resultMap.put("status", "500");
            resultMap.put("message", "session超时");
            return resultMap;
        }

        if (!VerifyCodeUtils.verifyCode(code)) {
            resultMap.put("status", "500");
            resultMap.put("message", "验证码错误");
            return resultMap;
        }

        Subject user = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            user.login(token);
            if (user.isAuthenticated()) {
                map.put("url", "index");
            }
        } catch (IncorrectCredentialsException e) {
            error = "登录密码错误.";
        } catch (ExcessiveAttemptsException e) {
            error = "登录失败次数过多";
        } catch (LockedAccountException e) {
            error = "帐号已被锁定.";
        } catch (DisabledAccountException e) {
            error = "帐号已被禁用.";
        } catch (ExpiredCredentialsException e) {
            error = "帐号已过期.";
        } catch (UnknownAccountException e) {
            error = "帐号不存在";
        } catch (UnauthorizedException e) {
            error = "您没有得到相应的授权！";
        }

        if (StringUtils.isBlank(error)) {
            resultMap.put("status", "200");
            resultMap.put("message", "success");
            return resultMap;
        } else {
            resultMap.put("status", "500");
            resultMap.put("message", error);
            return resultMap;
        }
    }

    @GetMapping("index")
    public String showView(Model model) {
        return "index";
    }


    /**
     * 获取验证码（Gif版本）
     *
     * @param response
     */
    @RequestMapping(value = "/getGifCode", method = RequestMethod.GET)
    public void getGifCode(HttpServletResponse response) {
        try {
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("image/gif");
            /**
             * gif格式动画验证码
             * 宽，高，位数。
             */
            Captcha captcha = new GifCaptcha(146, 42, 4);
            //输出
            ServletOutputStream out = response.getOutputStream();
            captcha.out(out);
            out.flush();
            //存入Shiro会话session
            String vcode = captcha.text().toLowerCase();
            System.out.println(vcode);
            TokenManager.setVal2Session(VerifyCodeUtils.V_CODE, vcode);
        } catch (Exception e) {

        }
    }

	/*public static void main(String args[]){
		LOGGER.info("result的值为"+HttpUtil.get("http://localhost:8080/static/admin/quartzTask/list"));
	}*/

    @GetMapping("main")
    public String main(Model model) {
        showStatistics(model);
        return "main";
    }

    /**
     * @author Chris
     * @date 2018/7/15 19:09
     * @todo 装饰器入口
     **/
    @RequestMapping(value = "/decorator")
    public String decorator() {
        System.out.println("测试环境***************");
        return "decorator";
    }

    private void showStatistics(Model model) {
        User user = userService.findUserById(MySysUser.id());
        Set<Menu> menus = user.getMenus();
        java.util.List<Menu> showMenus = Lists.newArrayList();
        if (menus != null && menus.size() > 0) {
            for (Menu menu : menus) {
                if (StringUtils.isNotBlank(menu.getHref())) {
                    StringBuilder mys = new StringBuilder("http://localhost");
                    if ("80".equals(port)) {
                        mys.append("/static");
                    } else {
                        mys.append(":").append(port).append("/static");
                    }
                    mys.append(menu.getHref());
                    String result = HttpUtil.get(mys.toString());
                    if (StringUtils.isNotBlank(result)) {
                        menu.setDataCount(Integer.valueOf(result));
                        showMenus.add(menu);
                    }
                }
            }
        }
        showMenus.sort(new MenuComparator());
        model.addAttribute("userMenu", showMenus);
    }

    /**
     * 空地址请求
     *
     * @return
     */
    @GetMapping(value = "")
    public String index() {
        LOGGER.info("这事空地址在请求路径");
        Subject s = SecurityUtils.getSubject();
        return s.isAuthenticated() ? "redirect:index" : "login";
    }

    @GetMapping("systemLogout")
    @SysLog("退出系统")
    public String logOut() {
        SecurityUtils.getSubject().logout();
        return "redirect:/login";
    }

    /**
     * 统计用户
     *
     * @return
     */
    @GetMapping("/static/admin/system/user/list")
    @ResponseBody
    public Integer getUserStatistics() {
        return userService.selectCount(new EntityWrapper<User>().eq("del_flag", false));
    }

    /**
     * 统计角色
     *
     * @return
     */
    @GetMapping("/static/admin/system/role/list")
    @ResponseBody
    public Integer getRoleStatistics() {
        return roleService.selectCount(new EntityWrapper<Role>().eq("del_flag", false));
    }

    /**
     * 统计菜单
     *
     * @return
     */
    @GetMapping("/static/admin/system/menu/list")
    @ResponseBody
    public Integer getMenuStatistics() {
        return menuService.selectCount(new EntityWrapper<Menu>().eq("del_flag", false));
    }

    /**
     * 统计资源
     *
     * @return
     */
    @GetMapping("/static/admin/system/rescource/list")
    @ResponseBody
    public Integer getRescourceStatistics() {
        return rescourceService.selectCount(new EntityWrapper<Rescource>().eq("del_flag", false));
    }

    /**
     * 统计日志
     *
     * @return
     */
    @GetMapping("/static/admin/system/log/list")
    @ResponseBody
    public Integer getLogStatistics() {
        return logService.selectCount(new EntityWrapper<Log>().eq("del_flag", false));
    }

    /**
     * 统计站点
     *
     * @return
     */
    @GetMapping("/static/admin/system/site/show")
    @ResponseBody
    public Integer getSiteStatistics() {
        return 15;
    }

    /**
     * 统计数据库
     *
     * @return
     */
    @GetMapping("/static/admin/system/table/list")
    @ResponseBody
    public Integer getTableStatistics() {
        return tableService.getTableCount();
    }

    /***
     * 统计字典
     * @return
     */
    @GetMapping("/static/admin/system/dict/list")
    @ResponseBody
    public Integer getDictStatistics() {
        return dictService.selectCount(new EntityWrapper<Dict>().eq("del_flag", false));
    }


}

class MenuComparator implements Comparator<Menu> {

    @Override
    public int compare(Menu o1, Menu o2) {
        if (o1.getSort() > o2.getSort()) {
            return -1;
        } else {
            return 0;
        }

    }
}