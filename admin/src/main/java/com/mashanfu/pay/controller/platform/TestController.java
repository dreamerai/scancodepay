package com.mashanfu.pay.controller.platform;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mashanfu.pay.entity.Test;
import com.mashanfu.pay.service.TestService;
import com.mashanfu.pay.util.CreateTableFiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 测试一下mybatisplus插件
 */
@Controller
public class TestController {

    @Autowired
    private TestService testService;

    @Autowired
    private CreateTableFiles createTableFiles;


    @RequestMapping(value = "/test")
    public String getList(){


        EntityWrapper<Test> organizationEntityEntityWrapper = new EntityWrapper<>();

        Page<Test> userPage = testService.selectPage(new Page<>(1,10),organizationEntityEntityWrapper);
        System.out.println("page+"+userPage);
        String [] str={"organization"};
        createTableFiles.createFile(str,1);

        return "";

    }


}
