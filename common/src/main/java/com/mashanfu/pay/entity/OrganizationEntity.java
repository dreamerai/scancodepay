package com.mashanfu.pay.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mashanfu.pay.base.DataEntity;
import lombok.Data;

import java.util.Date;

@TableName("organization")
public @Data class OrganizationEntity extends DataEntity<OrganizationEntity> {
    private static final long serialVersionUID = 1L;

    /**
     * 代理商ID
     */
    @TableField("Agent_Id")
    private Integer AgentId;
    /**
     * 商户名称
     */
    @TableField("Business_Name")
    private String BusinessName;
    /**
     * 商户代码
     */
    @TableField("Org_Code")
    private String OrgCode;
    /**
     * 联系人
     */
    @TableField("Contact")
    private String Contact;
    /**
     * 联系人号码
     */
    @TableField("Contact_Mobile")
    private String ContactMobile;
    /**
     * 0正常 1禁用
     */
    @TableField("Status")
    private Integer Status;
    /**
     * 审核状态0通过 1未审核 2不通过
     */
    @TableField("Approve_State_Code")
    private Integer ApproveStateCode;
    /**
     * 登录名
     */
    @TableField("User_Name")
    private String UserName;
    /**
     * 密码(MD5加密)
     */
    @TableField("Password")
    private String Password;
    /**
     * MD5秘钥
     */
    @TableField("Secret_Key")
    private String SecretKey;
    /**
     * DES加密秘钥
     */
    @TableField("DES_Secret_Key")
    private String DESSecretKey;
    /**
     * 总金额
     */
    @TableField("Amount")
    private Long Amount;
    /**
     * 可用金额
     */
    @TableField("Available_Amount")
    private Long AvailableAmount;
    /**
     * 冻结金额
     */
    @TableField("Freeze_Amount")
    private Long FreezeAmount;
    /**
     * 预留金额
     */
    @TableField("Reserved_Amount")
    private Long ReservedAmount;
    /**
     * 登录绑定IP以 , 分隔允许多个 IP 白名单
     */
    @TableField("Login_Bind_IP")
    private String LoginBindIP;
    /**
     * 绑定IP启用状态 0启用 1不启用
     */
    @TableField("Bind_IP_status")
    private Integer BindIPStatus;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 描述
     */
    @TableField("Description")
    private String Description;
    /**
     * 最后一次登录时间
     */
    @TableField("Last_Login_Time")
    private Date LastLoginTime;
    /**
     * 最后一次登录IP
     */
    @TableField("Last_Login_IP")
    private String LastLoginIP;
    /**
     * 登录失败次数
     */
    @TableField("Login_Faild_Count")
    private Integer LoginFaildCount;
    /**
     * 创建时间
     */
    private Date createDate;
    /**
     * 创建者-jeesite框架强制要求字段
     */
    private String createBy;
    /**
     * 更新者-jeesite框架强制要求字段
     */
    private String updateBy;
    /**
     * 更新时间-jeesite框架强制要求字段
     */
    private Date updateDate;
    /**
     * 是否是后台收款商户
     */
    private String preparationOne;
    /**
     * 初始密码
     */
    private String preparationTwo;
    private String preparationThree;
    /**
     * 是否绑定上游商户（0 未绑定; 1 已经绑定）
     */
    @TableField("is_binding_Upstream_mer")
    private Integer isBindingUpstreamMer;
    /**
     * 上游商户账号
     */
    private String upstreamMerAccount;
    /**
     * 绑定谷歌验证器
     */
    private Integer googleStatus;
    /**
     * 谷歌验证码
     */
    private String googleKey;


}
