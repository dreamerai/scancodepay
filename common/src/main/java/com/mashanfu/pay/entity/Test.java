package com.mashanfu.pay.entity;


import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@TableName("test")
public @Data  class Test {
    private String name;
    private String password;
}
