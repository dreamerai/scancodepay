package com.mashanfu.pay.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mashanfu.pay.dao.TestDao;
import com.mashanfu.pay.entity.Test;
import com.mashanfu.pay.service.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl extends ServiceImpl<TestDao, Test> implements TestService {
}
