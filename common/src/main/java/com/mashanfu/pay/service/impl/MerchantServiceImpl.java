package com.mashanfu.pay.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mashanfu.pay.dao.MerchantDao;
import com.mashanfu.pay.entity.OrganizationEntity;
import com.mashanfu.pay.service.MerchantService;
import com.mashanfu.pay.util.page.Pagination;
import org.springframework.stereotype.Service;

/**
 *   查询商户列表
 */

@Service
public class MerchantServiceImpl extends ServiceImpl<MerchantDao, OrganizationEntity> implements MerchantService {

    public Pagination<OrganizationEntity> getList(Integer pageNo, Integer pageSize, EntityWrapper organizationEntityEntityWrapper) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;

        Page<OrganizationEntity> page = this.selectPage(new Page<>(pageNo, pageSize), organizationEntityEntityWrapper);

        Pagination<OrganizationEntity> pagination = new Pagination();
        pagination.setList(page.getRecords());
        pagination.setTotalCount(page.getTotal());
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;

    }


}
