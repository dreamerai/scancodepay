package com.mashanfu.pay.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.IService;
import com.mashanfu.pay.entity.OrganizationEntity;
import com.mashanfu.pay.util.page.Pagination;


public interface MerchantService extends IService<OrganizationEntity> {

    Pagination<OrganizationEntity> getList(Integer pageNo, Integer pageSize, EntityWrapper organizationEntityEntityWrapper);
}
