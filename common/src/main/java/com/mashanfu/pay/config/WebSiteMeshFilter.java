package com.mashanfu.pay.config;


import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

/**
 * @author Chris
 * @date 2018/7/15 14:59
 * @todo  SiteMesh渲染配置
 **/
public class WebSiteMeshFilter extends ConfigurableSiteMeshFilter {

    public static final String DECORATOR_URL="/decorator";

    /**
     * decoratorPath 第一个参数配置被模板装饰的页面  第二个参数为模板页
     * @param builder
     */
    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
        builder.addDecoratorPath("/**", DECORATOR_URL)
                .addExcludedPath("/static/**")//不需要装饰的路径
                .addExcludedPath("/header")
                .addExcludedPath("")
                .addExcludedPath("/500")
                .addExcludedPath("/admin/error/404")
                .addExcludedPath("/login");
    }
}
