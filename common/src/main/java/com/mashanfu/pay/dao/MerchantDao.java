package com.mashanfu.pay.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mashanfu.pay.entity.OrganizationEntity;

public interface MerchantDao extends BaseMapper<OrganizationEntity> {


}
