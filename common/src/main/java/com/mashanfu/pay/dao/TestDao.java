package com.mashanfu.pay.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mashanfu.pay.entity.Test;

public interface TestDao extends BaseMapper<Test> {
}
